<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('domains_allowed_user_agents', function (Blueprint $table) {
            $table->bigInteger('domain_id')->unsigned();
            $table->bigInteger('ua_id')->unsigned();

            $table->unique(['domain_id', 'ua_id']);
            $table->foreign('domain_id')->references('id')->on('domains')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ua_id')->references('id')->on('allowed_user_agents')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('domains_allowed_user_agents');
    }
};
