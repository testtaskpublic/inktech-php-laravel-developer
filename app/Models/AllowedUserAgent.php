<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class AllowedUserAgent extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_agent',
    ];

    public function domains(): BelongsToMany
    {
        return $this->belongsToMany(
            Domain::class,
            'domains_allowed_user_agents',
            'ua_id',
            'domain_id'
        );
    }
}
