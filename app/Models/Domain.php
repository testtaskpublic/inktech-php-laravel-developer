<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\Builder;

/**
 * @mixin Builder
 */
class Domain extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
    ];

    public function allowedIps(): BelongsToMany
    {
        return $this->belongsToMany(
            AllowedIp::class,
            'domains_allowed_ips',
            'domain_id',
            'allowed_ip_id'
        );
    }

    public function allowedUserAgents(): BelongsToMany
    {
        return $this->belongsToMany(
            AllowedUserAgent::class,
            'domains_allowed_user_agents',
            'domain_id',
            'ua_id'
        );
    }
}
