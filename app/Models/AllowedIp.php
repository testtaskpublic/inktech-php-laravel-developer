<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class AllowedIp extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'ip_address',
    ];

    public function domains(): BelongsToMany
    {
        return $this->belongsToMany(
            Domain::class,
            'domains_allowed_ips',
            'allowed_ip_id',
            'domain_id'
        );
    }
}
