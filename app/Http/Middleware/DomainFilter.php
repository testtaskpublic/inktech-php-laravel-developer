<?php

namespace App\Http\Middleware;

use App\Models\Domain;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DomainFilter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $allowed = Domain::where('name', $request->getHost())
            ->whereHas('allowedIps', function ($builder) use ($request) {
                $builder->where('ip_address', '=', $request->ip());
            })
            ->whereHas('allowedUserAgents', function ($builder) use ($request) {
                $builder->where('user_agent', '=', $request->header('User-Agent'));
            })
            ->exists();

        if (!$allowed) {
            abort(404, 'Forbidden domain');
        }

        return $next($request);
    }
}
