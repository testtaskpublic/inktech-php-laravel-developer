<?php

namespace Tests\Unit\Middleware;

use App\Http\Middleware\DomainFilter;
use App\Models\AllowedIp;
use App\Models\AllowedUserAgent;
use App\Models\Domain;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;

class DomainFilterTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @test
     */
    public function it_does_not_allow_user_to_reach_domain(): void
    {
        Domain::factory()
            ->hasAttached(AllowedUserAgent::factory())
            ->hasAttached(AllowedIp::factory())
            ->create(["name" => "test1.com"]);

        $request = Request::create("http://test1.com");
        $request->headers->set("User-Agent", $this->faker()->unique()->userAgent());
        $request->server->add(['REMOTE_ADDR' => $this->faker->unique()->ipv4()]);

        $middleware = new DomainFilter();
        $this->expectException(HttpException::class);
        $middleware->handle($request, function ($request) {
            return response("Allowed!");
        });
    }

    /**
     * @test
     */
    public function it_allows_user_to_reach_domain(): void
    {
        $allowedIp = $this->faker->ipv4();
        $allowedUserAgent = $this->faker()->userAgent();

        $domain = Domain::factory()->create(["name" => "test2.com"]);
        $domain->allowedUserAgents()->attach(AllowedUserAgent::factory()->create(['user_agent' => $allowedUserAgent]));
        $domain->allowedIps()->attach(AllowedIp::factory()->create(['ip_address' => $allowedIp]));

        $request = Request::create("http://test2.com");
        $request->headers->set("User-Agent", $allowedUserAgent);
        $request->server->add(['REMOTE_ADDR' => $allowedIp]);

        $middleware = new DomainFilter();
        $response = $middleware->handle($request, function ($request) {
            return response("Allowed!");
        });

        self::assertEquals("Allowed!", $response->getContent());
    }
}