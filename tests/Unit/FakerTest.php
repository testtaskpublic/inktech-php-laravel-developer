<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FakerTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     */
    public function faker_is_not_failing(): void
    {
        $count = 90;

        $faker = $this->faker->unique();

        for ($i = 0; $i < $count; $i++) {
            $data[$faker->word()] = User::factory()->make();
        }

        $this->assertCount($count, $data);
    }
}
